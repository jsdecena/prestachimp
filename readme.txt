#This is a MailChimp module for prestashop

#Current method - list/subscribe

How to use:
1. Extract the prestachimp in your prestashop module folder.
2. Install via the back office.
3. You must provide the API for your Mailchimp and the List ID of your "mailchimp list".
4. You can put the override/classes/FrontController.php in your own folder override.
5. Now you can put this smarty variable "{$HOOK_MY_MAILCHIMP}" anywhere in your page.