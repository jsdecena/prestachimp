<div class="container">
	<div id="newsletter-wrap">
		<div class="col-lg-12">
			{if Tools::getValue('action') == "mc" && Tools::getValue('status') == 1}
				<p class="alert alert-success">You have successfully subscribed to the mailchimp!</p>
			{elseif Tools::getValue('action') == "mc" && Tools::getValue('status') == 0}
				<p class="alert alert-danger">{Tools::getValue('error')}</p>
			{/if}
		</div>
		<div class="col-lg-6">
			<h3>Subscribe to our Newsletter</h3>
		</div>
		<div class="col-lg-6 mc-form">
			<form action="{$mailchimp}" class="form-inline" role="form" method="post">
				<input class="form-control pull-left" name="EMAIL" id="EMAIL" type="email" placeholder="Enter email">
				<button type="submit" class="btn btn-warning pull-right">Subscribe</button>
			</form>
		</div>
	</div>
</div>